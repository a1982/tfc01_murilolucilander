package com.example.tfc01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var txtDI: TextView
    private lateinit var txtDH: TextView
    private var n1: Double = 0.0
    private var n2: Double = 0.0
    private lateinit var op: String
    private var control: Int = 0
    private var numero: Int = 0
    private var resultMode: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.txtDI = findViewById(R.id.txtDI)
        this.txtDH = findViewById(R.id.txtDH)
        this.resetValues()
    }

    fun onClickNumber(v: View){
        val digit = (v as Button).text.toString()

        if (this.resultMode) {
            if(digit == ",") {
                control = 1
                this.txtDI.text = "0."
                resultMode = false
            }else{
                this.txtDI.text = digit
                resultMode = false
            }
        } else {
            if(digit == ",") {
                if(control == 0){
                    control = 1
                    this.txtDI.append(".")
                }
            }else{
                this.txtDI.append(digit)
            }
        }
        this.lastButton(1)
    }

    fun onClickOperation(v: View){
        val opDigit = (v as Button).text.toString()
        val value = this.txtDI.text.toString().toDouble()

        if(this.op.isEmpty()){
            this.n1 = value
            this.op = opDigit
            this.resultMode = true
            if(txtDH.text.toString().isEmpty())
            {
                this.txtDH.text = "${this.txtDI.text} ${this.op}"
            }
        }else{
            if(numero == 1)//última tecla pressionada foi um número
            {
                this.n2 = value
                when(this.op){
                    "+" -> {this.n1 += this.n2}
                    "-" -> {this.n1 -= this.n2}
                    "x" -> {this.n1 *= this.n2}
                    "/" -> {this.n1 /= this.n2}
                }
                this.op = opDigit
                this.txtDH.text = "${this.txtDH.text} ${this.txtDI.text} ${this.op}"
                this.txtDI.text = "${this.n1}"
            }else{
                this.op = opDigit
                this.txtDH.text = "${this.txtDI.text} ${this.op}"
            }

        }
        this.resultMode = true
        this.lastButton(2)
        control = 0
    }

    fun resetValues(){
        this.txtDH.text = ""
        this.txtDI.text = "0"
        this.resultMode = true
        this.n1 = 0.0
        this.op = ""
        this.n2 = 0.0
        control = 0
        numero = 0
    }

    fun onClickC(v: View){
        this.resetValues()
    }

    fun lastButton(btn: Int){
        if(btn == 1)//numero pressionado por ultimo
        {
            numero = 1
        }else{
            numero = 0
        }
    }
    /*
    fun onClickBN(v: View){
        val digit = (v as Button).text.toString()
        this.txt01.text = digit
    }
    */

}